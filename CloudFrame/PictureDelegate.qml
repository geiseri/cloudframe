import QtQuick 2.7
import QtQuick.Controls 1.5
import QtQuick.Layouts 1.3
import Material 0.3 as Material

Material.Card {
    property alias progress: progressBar.value
    property alias localPath : picture.source
    property alias isLoading: progressBar.visible
    Image {
        id: picture
        fillMode: Image.PreserveAspectFit
        source: localPath
        anchors.fill: parent
    }
    Material.ProgressCircle {
        id: progressBar
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        minimumValue: 0
        maximumValue: 100
        color: Material.Theme.accentColor
        indeterminate: progressBar.value == 0
    }
}

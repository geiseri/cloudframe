import QtQuick 2.7
import QtQuick.Controls 1.5
import QtQuick.Layouts 1.3
import QtQuick.VirtualKeyboard 2.1
import Material 0.3 as Material

Item {
    id: login
    property alias url: url.text
    property alias username: username.text
    property alias password: password.text
    property alias button: updateButton

    ColumnLayout {
        id: fields
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 15
        spacing: 15
        Material.TextField {
            id: url
            text: "http://nextcloud.geekcentral.pub/remote.php/webdav/Photos"
            Layout.fillWidth: true
            placeholderText: qsTr("Server Address")
            floatingLabel: true
        }
        Material.TextField {
            id: username
            text: "geiseri"
            Layout.fillWidth: true
            placeholderText: qsTr("Username")
            floatingLabel: true
        }
        Material.TextField {
            id: password
            echoMode: 2
            Layout.fillWidth: true
            placeholderText: qsTr("Password")
            floatingLabel: true
        }
        Material.Button {
            id: updateButton
            text: qsTr("Update")
            isDefault: true
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
        }
    }
    InputPanel {
        id: keyboard
        anchors.top: fields.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.topMargin: 15
    }
}

#include <QApplication>
#include <QQmlApplicationEngine>

#include "quickwebdavdir.h"

int main(int argc, char *argv[])
{
    qputenv("QT_IM_MODULE", QByteArray("qtvirtualkeyboard"));

    QApplication app(argc, argv);

    QQmlApplicationEngine engine;
    qmlRegisterType<QuickWebdavDir>("QuickWebdav", 1, 0, "WebdavDir");

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}

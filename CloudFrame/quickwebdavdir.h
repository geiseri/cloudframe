#ifndef QUICKWEBDAVDIR_H
#define QUICKWEBDAVDIR_H

#include <QAbstractListModel>
#include <QDateTime>
#include <QUrl>
#include <QSslError>
#include <QJSValue>

class QTemporaryFile;

struct WebdavEntry {
    enum State {
        NotLoaded = 0,
        Loading = 1,
        Loaded = 2
    };

    QDateTime m_lastModified; // <d:getlastmodified>
    qulonglong m_size; // <d:getcontentlength>
    QString m_mimetype; // <d:getcontenttype> or <d:resourcetype><d:collection/></d:resourcetype> = directory
    QUrl m_href; // <d:href>
    QTemporaryFile *m_file; // locale cached file
    QDateTime m_lastAccess; // last access time
    State m_state; // load state
    int m_progress; // load progress

    ~WebdavEntry();
};

bool operator ==( const WebdavEntry &left, const WebdavEntry &right );

class QNetworkReply;
class QAuthenticator;
class QDomElement;
class WebdavAccessManager;

class QuickWebdavDir : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(int count READ count NOTIFY countChanged)
    Q_PROPERTY(QUrl url READ url WRITE setUrl NOTIFY urlChanged)
    Q_PROPERTY(QJSValue authenticator READ authenticator WRITE setAuthenticator NOTIFY authenticatorChanged)

public:
    explicit QuickWebdavDir(QObject *parent = Q_NULLPTR);
    ~QuickWebdavDir();

    enum WebdavRoles {
        LastModified = Qt::UserRole + 1,
        Size = Qt::UserRole + 2,
        MimeType = Qt::UserRole + 3,
        Href = Qt::UserRole + 4,
        LocalPath = Qt::UserRole + 5,
        IsLoading = Qt::UserRole + 6,
        LoadProgress = Qt::UserRole + 7,
    };

public:
    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    virtual QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;
    virtual QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;

    QUrl url() const;
    void setUrl(const QUrl &url);

    QJSValue authenticator() const;
    void setAuthenticator(const QJSValue &authenticator);

    int count() const;

Q_SIGNALS:
    void countChanged();
    void urlChanged();
    void authenticatorChanged();
    void sslErrors(const QStringList &errors);
    void error(const QString &error);

public Q_SLOTS:
    void update();
    void testConnection(const QJSValue &response);

private Q_SLOTS:
    void onAuthenticationRequired(QNetworkReply *reply, QAuthenticator *authenticator);
    void onSslErrors(QNetworkReply *reply, const QList<QSslError> &errors);

private:
    WebdavEntry parseResponse(const QDomElement &element) const;
    void requestUrlLoad(const WebdavEntry &entry );
    bool acceptEntry(const WebdavEntry &entry) const;

private:
    QUrl m_url;
    QList<WebdavEntry> m_entries;
    WebdavAccessManager *m_webdav;
    QJSValue m_authenticator;
};

#endif // QUICKWEBDAVDIR_H

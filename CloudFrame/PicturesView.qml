import QtQuick 2.7

ListView {
    id: pictures
    clip: true
    orientation: ListView.Horizontal
    snapMode: ListView.SnapOneItem
    keyNavigationWraps: true

    delegate: PictureDelegate {
        progress: model.progress
        localPath: model.localPath
        isLoading: model.isLoading
        width: pictures.width
        height: pictures.height
    }

    Timer {
        id: scrollTimer
        interval: 10000
        repeat: true
        running: pictures.count > 0

        onTriggered: {

            console.log("Current index: %1".arg(pictures.currentIndex))

            if ( pictures.currentIndex < 0 ) {
                pictures.incrementCurrentIndex();
            }

            if( !pictures.currentItem.isLoading ) {
                pictures.incrementCurrentIndex();
            }
        }
    }
}

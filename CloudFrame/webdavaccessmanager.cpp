#include "webdavaccessmanager.h"

#include <QDebug>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QBuffer>

WebdavAccessManager::WebdavAccessManager(QObject *parent) :
    QNetworkAccessManager(parent)
{

}

QNetworkReply *WebdavAccessManager::propfind(const QNetworkRequest &request, const QList<WebdavProperty> &props , int depth)
{

    QByteArray payload;
    QNetworkRequest propfindRequest = request;
    propfindRequest.setRawHeader("Depth", depth == 2 ? "infinity" : QByteArray::number(depth));

    payload = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><D:propfind xmlns:D=\"DAV:\" ><D:prop>";
    foreach( WebdavProperty prop, props) {
        payload += prop.value();
    }
    payload += "</D:prop></D:propfind>";

    QBuffer *data = new QBuffer(this);
    //data->setData(payload);
    data->open(QIODevice::ReadOnly);

    QNetworkReply *reply = sendCustomRequest(propfindRequest,"PROPFIND", data);
    connect(reply, &QNetworkReply::finished, [=] {
        qDebug() << Q_FUNC_INFO << "done";
        data->deleteLater();
    });

    return reply;
}

QByteArray WebdavProperty::value() const
{
    if ( m_namespace == "DAV:") {
        return "<D:" + m_key + "/>";
    } else {
        return "<" + m_key + " xmlns=\"" + m_namespace + "\"/>";
    }
}

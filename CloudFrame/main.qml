import QtQuick 2.7
import QtQuick.Controls 1.5
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.2
import Material 0.3 as Material
import QuickWebdav 1.0

Material.Window {
    visible: true
    title: qsTr("CloudFrame")

    Component.onCompleted: showMaximized()

    WebdavDir {
        id: dir
        url: login.url

        authenticator: function(realm) {
            console.log("auth %1".arg(realm));
            return  { username : login.username,
                password : login.password };
        }

        onError: {
            console.log("Error %1".arg(error));
        }

        onSslErrors: {
            console.log("SSL Errors: %1".arg(errors));
        }
    }

    PicturesView {
        id: pictures
        model: dir
        anchors.fill: parent
        focus: dir.count > 0
    }

    Material.Card {

        id: sheet
        elevation: 2
        height: parent.height - (8 * Material.Units.dp)
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        visible: dir.count < 1
        Login {
            id: login
            anchors.fill: parent
            button.onClicked: dir.update()
        }
    }

}

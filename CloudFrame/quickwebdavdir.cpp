#include "quickwebdavdir.h"
#include "webdavaccessmanager.h"

#include <QDebug>
#include <QAuthenticator>
#include <QNetworkReply>
#include <QDomDocument>
#include <QDomElement>
#include <QQmlContext>
#include <QQmlEngine>
#include <QTemporaryFile>

QuickWebdavDir::QuickWebdavDir(QObject *parent) : QAbstractListModel(parent), m_webdav( new WebdavAccessManager(this) )
{
    connect( m_webdav, &WebdavAccessManager::authenticationRequired, this, &QuickWebdavDir::onAuthenticationRequired);
    connect( m_webdav, &WebdavAccessManager::sslErrors, this, &QuickWebdavDir::onSslErrors);
    connect( m_webdav, &WebdavAccessManager::encrypted, this, [] (QNetworkReply *reply) {
        qDebug() << Q_FUNC_INFO << "encrypted" << reply->url();
    });
}

QuickWebdavDir::~QuickWebdavDir()
{

}

int QuickWebdavDir::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);

    return m_entries.length();
}

QVariant QuickWebdavDir::data(const QModelIndex &index, int role) const
{
    if( index.column() == 0 && index.row() < rowCount() ) {
        switch ( role ) {
        case LastModified:
            return QVariant::fromValue(m_entries[index.row()].m_lastModified);
            break;
        case Size:
            return QVariant::fromValue(m_entries[index.row()].m_size);
            break;
        case MimeType:
            return QVariant::fromValue(m_entries[index.row()].m_mimetype);
            break;
        case Href:
            return QVariant::fromValue(m_entries[index.row()].m_href);
            break;
        case LocalPath:
        {
            if ( m_entries[index.row()].m_mimetype != QLatin1String("inode/directory") ) {
                if( m_entries[index.row()].m_state == WebdavEntry::NotLoaded ) {
                    //TODO: Find a better way to update the local cache
                    const_cast<QuickWebdavDir*>(this)->requestUrlLoad( m_entries[index.row()] );
                } else if ( m_entries[index.row()].m_state == WebdavEntry::Loaded  ){
                    if ( m_entries[index.row()].m_file ) {
                        return QVariant::fromValue( QString(QLatin1String("file://%1")).arg(m_entries[index.row()].m_file->fileName()) );
                    }
                }
            }

            return QVariant::fromValue(QUrl(QLatin1String("qrc:/loading.svg")));

            break;
        }
        case IsLoading:
            return QVariant::fromValue(m_entries[index.row()].m_state == WebdavEntry::Loading);
            break;
        case LoadProgress:
            return QVariant::fromValue(m_entries[index.row()].m_progress);
            break;

        default:
            break;
        }
    }
    return QVariant();
}

QHash<int, QByteArray> QuickWebdavDir::roleNames() const
{
    QHash<int, QByteArray> names;
    names[LastModified] = "lastModified";
    names[Size] = "size";
    names[MimeType] = "mimeType";
    names[Href] = "href";
    names[LocalPath] = "localPath";
    names[IsLoading] = "isLoading";
    names[LoadProgress] = "progress";
    return names;
}

QUrl QuickWebdavDir::url() const
{
    return m_url;
}

void QuickWebdavDir::setUrl(const QUrl &url)
{
    if ( url != m_url ) {
        m_url = url;
        Q_EMIT urlChanged();
    }
}

WebdavEntry QuickWebdavDir::parseResponse( const QDomElement &element ) const
{
    /*
    <d:response>
      <d:href>/remote.php/webdav/</d:href>
      <d:propstat>
       <d:prop>
        <d:getlastmodified>Sun, 18 Dec 2016 15:21:31 GMT</d:getlastmodified>
        <d:resourcetype>
         <d:collection/>
        </d:resourcetype>
       </d:prop>
       <d:status>HTTP/1.1 200 OK</d:status>
      </d:propstat>
     </d:response>

     or

     <d:response>
      <d:href>/remote.php/webdav/Notes/utililite%20notes.txt</d:href>
      <d:propstat>
       <d:prop>
        <d:getlastmodified>Sat, 17 Sep 2016 14:26:47 GMT</d:getlastmodified>
        <d:getcontentlength>650</d:getcontentlength>
        <d:resourcetype/>
        <d:getetag>&quot;8ae2f21afe7dd379161ca19995e68f63&quot;</d:getetag>
        <d:getcontenttype>text/plain</d:getcontenttype>
       </d:prop>
       <d:status>HTTP/1.1 200 OK</d:status>
      </d:propstat>
     </d:response>

    */
    QString href = element.firstChildElement(QLatin1String("href")).text();

    QDomElement prop = element.firstChildElement(QLatin1String("propstat")).firstChildElement(QLatin1String("prop"));
    QDateTime getlastmodified = QDateTime::fromString(prop.firstChildElement(QLatin1String("getlastmodified")).text(), Qt::RFC2822Date);

    QString getcontenttype;
    qulonglong getcontentlength = 0;

    if ( !prop.firstChildElement(QLatin1String("resourcetype")).firstChildElement(QLatin1String("collection")).isNull() ) {
        getcontenttype = QLatin1String("inode/directory");
        getcontentlength = prop.firstChildElement( QLatin1String("quota-used-bytes") ).text().toULongLong();
    } else {
        getcontenttype = prop.firstChildElement( QLatin1String("getcontenttype") ).text();
        getcontentlength = prop.firstChildElement( QLatin1String("getcontentlength") ).text().toULongLong();
    }


    //qDebug() << Q_FUNC_INFO << href << getcontenttype << getcontentlength << getlastmodified;

    return WebdavEntry({
                           getlastmodified,
                           getcontentlength,
                           getcontenttype,
                           href,
                           Q_NULLPTR,
                           QDateTime(),
                           WebdavEntry::NotLoaded,
                           0
                       });
}

void QuickWebdavDir::requestUrlLoad(const WebdavEntry &entry)
{
    int idx = m_entries.indexOf( entry );
    if ( idx != -1 ) {

        m_entries[idx].m_state = WebdavEntry::Loading;
        dataChanged(index(idx,0),index(idx,0), {QuickWebdavDir::IsLoading});


        QUrl url = m_url;
        url.setPath(entry.m_href.path());
        QNetworkRequest request(url);
        QNetworkReply *reply = m_webdav->get(request);

        if( m_entries[idx].m_file ) {
            delete m_entries[idx].m_file;
        }

        m_entries[idx].m_file = new QTemporaryFile();
        m_entries[idx].m_file->open();

        connect( reply, &QNetworkReply::redirected, [=](const QUrl &url) {
            //TODO: how to handle this case?
            qDebug() << Q_FUNC_INFO << "redirected" << url;
        });

        connect( reply, &QNetworkReply::readyRead, [=]() {
            //qDebug() << Q_FUNC_INFO << "read" << url;
            m_entries[idx].m_file->write(reply->readAll());
        });

        connect( reply, &QNetworkReply::downloadProgress, [=](qint64 bytesReceived, qint64 bytesTotal) {
            m_entries[idx].m_progress = bytesReceived * 100 / bytesTotal;
            dataChanged(index(idx,0),index(idx,0), {QuickWebdavDir::LoadProgress});
        });

        connect( reply, &QNetworkReply::finished, [=]() {
            qDebug() << Q_FUNC_INFO << request.url() << "done";
            m_entries[idx].m_file->write(reply->readAll());
            m_entries[idx].m_state = WebdavEntry::Loaded;
            m_entries[idx].m_file->close();
            dataChanged(index(idx,0),index(idx,0), {QuickWebdavDir::IsLoading,QuickWebdavDir::LocalPath});

            reply->deleteLater();
        });

    }
}

QJSValue QuickWebdavDir::authenticator() const
{
    return m_authenticator;
}

void QuickWebdavDir::setAuthenticator(const QJSValue &authenticator)
{
    if ( !authenticator.strictlyEquals(m_authenticator) ) {
        m_authenticator = authenticator;
        Q_EMIT authenticatorChanged();
    }
}

int QuickWebdavDir::count() const
{
    return rowCount();
}

bool QuickWebdavDir::acceptEntry( const WebdavEntry &entry ) const
{
    if ( entry.m_mimetype == QLatin1String("image/jpeg")) return true;

    return false;
}

void QuickWebdavDir::update()
{
    QNetworkRequest request;
    request.setUrl(m_url);
    request.setAttribute(QNetworkRequest::FollowRedirectsAttribute, true);

    QNetworkReply *reply = m_webdav->propfind(request,QList<WebdavProperty>(), 2);

    connect( reply, &QNetworkReply::redirected, [=](QUrl url) {
        qDebug() << Q_FUNC_INFO << "redirected" << url;
        reply->deleteLater();
        setUrl(url);
        update();
    });

    connect( reply, &QNetworkReply::finished, [=](){

        if ( reply->error() == QNetworkReply::NoError ) {

            QDomDocument document;
            QString errorMessage;
            int errorLine = 0;
            int errorColumn = 0;

            //qDebug() << Q_FUNC_INFO << buffer;

            if ( document.setContent(reply->readAll(), true, &errorMessage, &errorLine, &errorColumn) ) {
                QDomElement documentElement = document.documentElement();
                /*
                <?xml version="1.0"?>
                <d:multistatus xmlns:d="DAV:" xmlns:s="http://sabredav.org/ns" xmlns:oc="http://owncloud.org/ns" xmlns:nc="http://nextcloud.org/ns">
                 <d:response/>
                </d:multistatus>
                */
                QDomElement node = documentElement.firstChildElement(QLatin1String("response"));  // first response


                beginResetModel();
                m_entries.clear();
                while(!node.isNull()) {
                    WebdavEntry entry = parseResponse( node );
                    if ( acceptEntry(entry) ) {
                        m_entries << entry;
                    }
                    node = node.nextSiblingElement(QLatin1String("response"));
                }
                endResetModel();
                Q_EMIT countChanged();
            } else {
                Q_EMIT error( errorMessage );
            }

        } else {
            Q_EMIT error( reply->errorString() );
        }

        reply->deleteLater();

    });
}

void QuickWebdavDir::testConnection(const QJSValue &response)
{
    QNetworkRequest request;
    request.setUrl(m_url);
    request.setAttribute(QNetworkRequest::FollowRedirectsAttribute, true);

    QNetworkReply *reply = m_webdav->head(request);

    connect( reply, &QNetworkReply::redirected, [=](QUrl url) {
        qDebug() << Q_FUNC_INFO << "redirected" << url;
        reply->deleteLater();
        setUrl(url);
        testConnection(response);
    });

    connect( reply, &QNetworkReply::finished, [=](){

        if ( response.isCallable() ) {
            QJSValueList args = { QJSValue(reply->error() == QNetworkReply::NoError), QJSValue(reply->errorString())};
            QJSValue localResponse = response;
            localResponse.call(args);
        }

        reply->deleteLater();
    });
}

void QuickWebdavDir::onAuthenticationRequired(QNetworkReply *reply, QAuthenticator *authenticator)
{

    qDebug() << Q_FUNC_INFO << "status" << reply->error() << authenticator->options()
             << authenticator->user() << authenticator->isNull();

    QString currentUsername = authenticator->user();
    QString currentPassword = authenticator->password();

    if ( m_authenticator.isCallable() ) {
        QJSValueList args = { QJSValue(authenticator->realm())};
        QVariantMap result = m_authenticator.call(args).toVariant().toMap();

        if ( result.contains(QLatin1String("username")) ) {
            authenticator->setUser(result[QLatin1String("username")].toString());
        }

        if ( result.contains(QLatin1String("password")) ) {
            authenticator->setPassword(result[QLatin1String("password")].toString());
        }
    }

    if ( currentPassword == authenticator->password() &&
         currentUsername == authenticator->user() ) {
        reply->abort();
    }
}

void QuickWebdavDir::onSslErrors(QNetworkReply *reply, const QList<QSslError> &errors)
{
    Q_UNUSED(reply);

    QStringList errorStrings;
    foreach( QSslError error, errors) {
        errorStrings << error.errorString();
    }
    Q_EMIT sslErrors(errorStrings);
}

bool operator ==(const WebdavEntry &left, const WebdavEntry &right)
{
    return left.m_href == right.m_href;
}

WebdavEntry::~WebdavEntry() {
    if ( m_file ) delete m_file;
}

#ifndef WEBDAVACCESSMANAGER_H
#define WEBDAVACCESSMANAGER_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QVariantMap>

class QNetworkReply;
class QNetworkRequest;

struct WebdavProperty {
    QByteArray m_key;
    QByteArray m_namespace;
    QByteArray value() const;
};

class WebdavAccessManager : public QNetworkAccessManager
{
public:
    WebdavAccessManager(QObject *parent = Q_NULLPTR);

    QNetworkReply *propfind(const QNetworkRequest &request, const QList<WebdavProperty> &props = QList<WebdavProperty>(), int depth = 1);

};

#endif // WEBDAVACCESSMANAGER_H
